<?php
/**
 * Outil pour déployer (mettre à jour le Kohinos) en une ligne de commande.
 *
 * dep deploy MLC_NAME
 *
 * MLC_NAME = le nom de votre mlc configuré dans le fichier ci-dessous
*/

namespace Deployer;

use Symfony\Component\Dotenv\Dotenv;

require_once __DIR__ . '/vendor/autoload.php';

$dotenv = new Dotenv();
$dotenv->loadEnv(__DIR__ . '/.env');

require 'recipe/symfony4.php';
require 'recipe/yarn.php';
require 'recipe/npm.php';

// Project name
set('application', 'Kohinos');
set('symfony_env', 'prod'); // Pour installer un environnement de test/développements mettre ici 'dev'

set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader');

// Project repository : vous pouvez configurer une clé SSH pour que votre serveur puisse accéder directement au gitlab de la fédération ou alors mettre ici votre LOGIN_GITLAB et votre MOT_DE_PASSE_GITLAB (Attention à ne pas partager le fichier !)
// set('repository', 'https://LOGIN_GITLAB:MOT_DE_PASSE_GITLAB@gitlab.com/federation-kohinos/kohinos.git');
set('repository', 'git@gitlab.com:federation-kohinos/kohinos.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

set('http_user', 'USER_SSH'); // Mettre ici le nom de votre user SSH 

set('writable_mode', 'chmod');

// Si le composer est long sur le serveur, on peut décommenter cette partie là !
// set('env', [
//     'COMPOSER_MEMORY_LIMIT' => '-1',
// ]);

set('branch', 'master');

// Shared files/dirs between deploys
add('shared_dirs', ['templates/themes/custom', 'public/images', 'public/csv', 'public/leaflet', 'public/fontawesome', 'public/js', 'public/upload/media', 'transactions']); //, 'public/documents', 'var/import', 'var/log', 'var/sessions', 'var/storage']);
// add('shared_files', ['config/jwt/private.pem', 'config/jwt/public.pem']);

// Writable dirs by web server
add('writable_dirs', ['public/uploads', 'var']);
set('allow_anonymous_stats', false);
task('deploy:writable', '');

// Releases
set('keep_releases', 3); // Nombre de releases que l'on veut garder (si on veut rollback/retourner à une release précédente)

// Hosts
$rootDir = '/home/XXXXXX/public_html/'; // Mettre ici votre dossier commun (avec un ou plusieurs Kohinos par exemple)
$mlcs = [
    'MLC_NAME' => $rootDir . 'sous_dossier/kohinos', // on peut avoir ici un dossier pour le Kohinos de production
    'MLC_NAME_TEST' => $rootDir . 'sous_dossier2/kohinos', // et ici un dossier pour un autre Kohinos de test (et on déploiera avec la commande : "dep deploy MLC_NAME_TEST")
];
foreach ($mlcs as $mlcName => $path) {
    host($mlcName)
            ->hostname('MLC_HOST_NAME.fr') // Mettre ici votre hostname (url d'accès au serveur, exemple : une-mlc.fr)
            ->user('USER_SSH') // Mettre ici le nom de votre user SSH 
            ->set('branch', 'master')
            ->set('deploy_path', $path)
        ;
    set('shared_files', ['.env']);
}

// Clean composer cache
task('deploy:composer:cache', function () {
    run('composer clearcache');
});
before('deploy:vendors', 'deploy:composer:cache');

// Deploy asset via Encore
task('deploy:yarn:build', function () {
    run('cd {{release_path}} && {{bin/yarn}} encore prod'); // Mettre ici "encore dev" si on veut déployer les assets de dev
});

// Unlock if failed
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
before('deploy:symlink', 'database:migrate');

// Remove .git config after success
after('success', 'khn:removeconfig');
task('khn:removeconfig', function () {
    run('cd {{release_path}} && rm .git/config');
});


/*
 * DECOMMENTER CECI SI C'EST UNE PREMIERE INSTALLATION DU KOHINOS
 */
// task('database:fixture1', function () {
//     run('cd {{release_path}} && {{bin/php}}  bin/console hautelook:fixtures:load --purge-with-truncate --env=pro');
// });
// task('database:fixture2', function () {
//     run('cd {{release_path}} && {{bin/php}}  bin/console sonata:media:fix-media-context');
// });
// task('database:fixture3', function () {
//     run('cd {{release_path}} && {{bin/php}}  bin/console hautelook:fixtures:load --append --env=pro');
// });
// after('database:migrate', 'database:fixture1');
// after('database:migrate', 'database:fixture2');
// after('database:migrate', 'database:fixture3');



/**
 * SOME COMMANDS NOT NECESSARY :
 */

/*
 * TRANSLATION IMPORT EN DB => non nécessaire/fonctionnel pour le moment !
 */
// after('database:migrate', 'translation:fr');
// after('database:migrate', 'translation:en');
// after('database:migrate', 'translation:import');
// task('translation:fr', function () {
//     run('{{bin/php}}  bin/console translation:update --force fr --prefix=""');
// });
// task('translation:en', function () {
//     run('{{bin/php}}  bin/console translation:update --force en --prefix=""');
// });
// task('translation:import', function () {
//     run('{{bin/php}}  bin/console lexik:translations:import --case-insensitive --force -c');
// });

/*
 * Update GPDR DB (for IBAN)
 */
// after('database:migrate', 'gdpr:update');
// task('gdpr:update', function () {
//     run('{{bin/php}} {{bin/console}} gdpr:update');
// });

/*
 * Create Account
 */
// after('database:migrate', 'kohinos:createacccount');
// task('kohinos:createacccount', function () {
//     run('{{bin/php}} {{bin/console}} kohinos:accounts:create');
// });
